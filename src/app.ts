require('dotenv').config();

import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as HttpStatus from 'http-status-codes';
import * as express from 'express';

import * as Knex from 'knex';
import { MySqlConnectionConfig } from 'knex';

const signale = require('signale');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const cors = require('cors');

import { Router, Request, Response, NextFunction } from 'express';

// Import routings
import indexRoute from './routes/index';
import userRoute from './routes/users';
import loginRoute from './routes/login';

const app: express.Application = express();

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});

var connection: MySqlConnectionConfig = {
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD
}

var hdcConnection: MySqlConnectionConfig = {
  host: 'localhost',
  port: 3307,
  database: 'hdc',
  user: 'root',
  password: '##devmate##'
}

var db: Knex = Knex({
  client: 'mysql',
  connection: connection,
  pool: {
    min: 0,
    max: 100,
    afterCreate: (conn, done) => {
      conn.query('SET NAMES utf8', (err) => {
        done(err, conn)
      })
    }
  }
});

var dbHDC: Knex = Knex({
  client: 'mysql',
  connection: hdcConnection,
  pool: {
    min: 0,
    max: 100,
    afterCreate: (conn, done) => {
      conn.query('SET NAMES utf8', (err) => {
        done(err, conn)
      })
    }
  }
});

app.use((req: Request, res: Response, next: NextFunction) => {
  req.logger = signale;
  next();
});

app.use(limiter);
app.use(helmet({ hidePoweredBy: { setTo: 'PHP 4.2.0' } }));
app.use(cors());

// app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.get('/favicon.ico', (req: Request, res: Response) => res.status(204));

app.use((req, res, next) => {
  req.db = db;
  req.dbHDC = dbHDC;
  next();
});

app.use('/', indexRoute);
app.use('/users', userRoute);
app.use('/login', loginRoute);
//error handlers

if (process.env.NODE_ENV === 'development') {
  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    req.logger.error(err.stack);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      error: {
        ok: false,
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
      }
    });
  });
}

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(HttpStatus.NOT_FOUND).json({
    error: {
      ok: false,
      code: HttpStatus.NOT_FOUND,
      error: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
    }
  });
});

export default app;
