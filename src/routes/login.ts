/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';

import * as crypto from 'crypto';
import { LoginModel } from '../models/login';
import { Jwt } from '../models/jwt';

const jwt = new Jwt();
const loginModel = new LoginModel();
const router: Router = Router();

// /login
router.post('/', async (req: Request, res: Response) => {
  const username: string = req.body.username;
  const password: string = req.body.password;

  try {
    const encPass = crypto.createHash('md5').update(password).digest('hex')
    const rs: any = await loginModel.login(req.db, username, encPass)

    if (rs.length) {
      var payload = {
        id: rs[0].id,
        fullname: rs[0].fullname
      }

      var token = jwt.sign(payload);
      res.send({ ok: true, token: token });
    } else {
      res.send({ ok: false, error: "ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง" })
    }

  } catch (error) {
    res.send({ ok: false, error: "เกิดข้อผิดพลาด" })
  }

});

export default router;