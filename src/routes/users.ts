/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as crypto from 'crypto';

import { UserModel } from '../models/user';

const userModel = new UserModel();

const router: Router = Router();

//RESTful
router.get('/detail', async (req: Request, res: Response) => {

  try {
    var rs: any = await userModel.getList(req.db);
    res.send({ ok: true, rows: rs[0] });
  } catch (error) {
    req.logger.error(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด', code: 500 })
  }

});

router.post('/detail', async (req: Request, res: Response) => {
  const username: string = req.body.username;
  const password: string = req.body.password;
  const fname: string = req.body.fname;
  const lname: string = req.body.lname;

  try {
    var data: any = {
      username: username,
      password: password,
      fullname: fname + ' ' + lname
    };
    var rs: any = await userModel.save(req.db, data);
    res.send({ ok: true });
  } catch (error) {
    req.logger.error(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด', code: 500 })
  }

});
// /detail?id=20
router.put('/detail', async (req: Request, res: Response) => {
  const id: any = req.query.id;
  const password: string = req.body.password;
  const fname: string = req.body.fname;
  const lname: string = req.body.lname;

  try {

    var encPass = crypto.createHash('md5').update(password).digest('hex');

    var data: any = {
      password: encPass,
      fullname: fname + ' ' + lname
    };

    await userModel.update(req.db, id, data);
    res.send({ ok: true });
  } catch (error) {
    req.logger.error(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด', code: 500 })
  }

});

// /detail?id=20
router.delete('/detail', async (req: Request, res: Response) => {
  const id: string = req.query.id;
  try {
    if (id) {
      await userModel.delete(req.db, id)
      res.send({ ok: true })
    } else {
      res.send({ ok: false, error: 'ไม่พบ ID' })
    }
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด' })
  }
});

router.get('/', (req: Request, res: Response) => {
  res.send({ ok: true, message: 'User management!', code: HttpStatus.OK });
});

router.get('/list', (req: Request, res: Response) => {
  const a: number = req.query.a;
  const b: number = req.query.b;

  req.logger.error('A = ' + a);
  req.logger.info('B = ' + b);
  console.log(a);
  console.log(b);

  res.send({ ok: true, message: 'user list', code: HttpStatus.OK });
});

// localhost:3000/info/man/20
router.get('/info/:sex/:age', (req: Request, res: Response) => {
  const sex: string = req.params.sex;
  const age: number = req.params.age;

  req.logger.info('Sex = ' + sex);
  req.logger.info('Age = ' + age);

  res.send({ ok: true, message: 'user list', code: HttpStatus.OK });
});

export default router;