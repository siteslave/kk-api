import * as Knex from 'knex'

export class LoginModel {
  login(db: Knex, username: string, password: string) {
    return db('users')
      .where({ username: username, password: password })
      .limit(1)
  }
}