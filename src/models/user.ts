import * as Knex from 'knex';

export class UserModel {

  save(db: Knex, data: any) {
    return db('users').insert(data);
  }

  update(db: Knex, id: any, data: any) {
    return db('users').update(data).where('id', id);
  }

  delete(db: Knex, id: any) {
    return db('users').where('id', id).del();
  }

  getList(db: Knex) {
    /**
     * SELECT id, fullname, username 
     * FROM users
     * WHERE id=1
     * AND username='admin'
     * AND (x='xx' OR y='yy');
     */

    // return db('users')
    //   .select('id', 'fullname', 'username')
    //   .where('id', 1)
    //   .where('username', 'admin')
    //   .where(w => w.where('x', 'xx').orWhere('y', 'yy'));

    let sql = `
    SELECT id, fullname, username 
    FROM users
    `;
    return db.raw(sql);
  }

}